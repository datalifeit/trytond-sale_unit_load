# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields
from trytond.modules.stock_unit_load import cases_digits
from sql.aggregate import Sum
from sql.conditionals import Coalesce


class Product(metaclass=PoolMeta):
    __name__ = 'sale.reporting.product'

    cases_quantity = fields.Float('Cases',
        digits=cases_digits)
    ul_quantity = fields.Float('ULs', digits=(16, 0))

    @classmethod
    def _columns(cls, tables, withs):
        line = tables['line']
        return super()._columns(tables, withs) + [
            Sum(Coalesce(line.cases_quantity, 0)).as_('cases_quantity'),
            Sum(Coalesce(line.ul_quantity, 0)).as_('ul_quantity')
        ]
