# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction
from sql.operators import Like


class UnitLoad(metaclass=PoolMeta):
    __name__ = 'stock.unit_load'

    customer = fields.Function(fields.Many2One('party.party', 'Customer'),
        'get_customer')
    sale = fields.Function(fields.Many2One('sale.sale', 'Sale'), 'get_sale',
        searcher='search_sale')
    sale_line = fields.Many2One('sale.line', 'Sale line', readonly=True)

    @classmethod
    def __register__(cls, module_name):
        cursor = Transaction().connection.cursor()
        table = cls.__table__()
        Move = Pool().get('stock.move')
        move_table = Move.__table__()

        tableh = cls.__table_handler__(module_name)
        sale_line_column_exists = tableh.column_exist('sale_line')

        super().__register__(module_name)

        if not sale_line_column_exists:
            cursor.execute(*table.join(move_table, condition=(
                table.id == move_table.unit_load)).select(
                    table.id, move_table.origin,
                    where=Like(move_table.origin, 'sale.line,%')))
            for ul_id, origin in cursor.fetchall():
                line_id = origin.split(',')[1]
                cursor.execute(*table.update(
                    columns=[table.sale_line],
                    values=[line_id],
                    where=table.id == ul_id))

    def get_customer(self, name=None):
        if self.shipment and self.shipment.__name__ == 'stock.shipment.out':
            return self.shipment.customer.id
        return None

    def get_sale(self, name=None):
        if self.sale_line:
            return self.sale_line.sale.id
        return None

    @classmethod
    def search_sale(cls, name, clause):
        _field = 'sale_line.%s' % clause[0]

        if '.' not in clause[0]:
            if isinstance(clause[2], (int, list, tuple, set)):
                _field += '.id'
            else:
                _field += '.rec_name'
        return [(_field, ) + tuple(clause[1:])]

    def _set_shipment(self, move):
        super()._set_shipment(move)
        if not self.shipment or \
                self.shipment.__name__ == 'stock.shipment.out.return':
            self.sale_line = None
        if self.shipment and self.shipment.__name__ == 'stock.shipment.out':
            sale_line = None
            if move.origin and move.origin.__name__ == 'sale.line':
                sale_line = move.origin
            elif (move.origin and move.origin.origin
                    and move.origin.origin.__name__ == 'sale.line'):
                # check outgoing moves
                sale_line = move.origin.origin
            self.sale_line = sale_line
