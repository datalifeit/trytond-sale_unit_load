# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import sale
from . import unit_load
from . import cost
from . import sale_reporting


def register():
    Pool.register(
        sale.Sale,
        sale.SaleLine,
        unit_load.UnitLoad,
        sale_reporting.Product,
        sale.ReturnSaleStart,
        sale.ReturnSaleStartLine,
        module='sale_unit_load', type_='model')
    Pool.register(
        sale.ReturnSale,
        module='sale_unit_load', type_='wizard')
    Pool.register(
        cost.CostType,
        cost.CostTemplate,
        cost.CostSale,
        cost.CostLineSale,
        module='sale_unit_load', type_='model', depends=['sale_cost'])
    Pool.register(
        sale.SaleLineQuickActionSplit,
        module='sale_unit_load', type_='model',
        depends=['sale_line_quick_actions'])
    Pool.register(
        sale.SaleLineQuickAction,
        module='sale_unit_load', type_='wizard',
        depends=['sale_line_quick_actions'])
